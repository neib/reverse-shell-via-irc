#ifndef CONNECT_HPP_INCLUDED
#define CONNECT_HPP_INCLUDED

using boost::asio::ip::tcp;
namespace ssl = boost::asio::ssl;
typedef ssl::stream<tcp::socket> ssl_socket;

ssl_socket connectTo(const std::string network, const std::string port);

#endif
