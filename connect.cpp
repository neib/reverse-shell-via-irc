#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

using boost::asio::ip::tcp;
namespace ssl = boost::asio::ssl;
typedef ssl::stream<tcp::socket> ssl_socket;


ssl_socket connectTo(const std::string network, const std::string port)
{
    // Create a context that uses the default paths for
    // finding CA certificates.
    ssl::context ctx(ssl::context::sslv23);
    ctx.set_default_verify_paths();

    // Open a socket and connect it to the remote host.
    boost::asio::io_context io_context;
    ssl_socket sock(io_context, ctx);
    tcp::resolver resolver(io_context);
    tcp::resolver::query query(network, port);
    boost::asio::connect(sock.lowest_layer(), resolver.resolve(query));
    sock.lowest_layer().set_option(tcp::no_delay(true));

    // Perform SSL handshake and verify the remote host's
    // certificate.
    //sock.set_verify_mode(ssl::verify_peer); // Too many troubles on IRC.
    sock.set_verify_mode(ssl::verify_none);
    sock.set_verify_callback(ssl::rfc2818_verification(network));
    sock.handshake(ssl_socket::client);

    return sock;
}
